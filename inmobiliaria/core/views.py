from django.shortcuts import render, redirect
from .models import Comuna, Provincia, Region, TipoInmueble, Zona, Inmueble, Nuevo
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from .form import InmuebleForm, CustomUserForm
from django.contrib.auth.decorators import login_required,permission_required
from django.contrib.auth import login, authenticate
from django.db.models import Q
# Create your views here.


def home(request):
    inmueble = Inmueble.objects.all()
    region = Region.objects.all()
    comuna= Comuna.objects.all()
    tipo= TipoInmueble.objects.all()


    data = {
        'inmuebles': inmueble,
        'region':region,
        'comuna':comuna,
        'tipo':tipo,
        # 'inmueblebuscar':inmueblebuscar
    }
    return render(request, 'core/home.html',data)

# @permission_required('core.add_inmueble')
def nuevoFormulario(request):
    data= {
        'form':InmuebleForm()
    }

    if request.method == 'POST':
        formulario = InmuebleForm(request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Guardado"
        data['form'] = formulario

    return render(request,'core/nuevoFormulario.html', data)

# @permission_required('core.change_inmueble')
def listar_inmuebles(request):
    inmueble = Inmueble.objects.all()

    data = {
        'inmuebles': inmueble
    }

    return render(request, 'core/listar_inmuebles.html', data)


def eliminar_inmueble(request, id):
    # buscar inmueble
    inmueble = Inmueble.objects.get(id=id)

    try:
        inmueble.delete()
        mensaje = "Eliminado correctamente"
        messages.success(request, mensaje)
    except:
        mensaje = "No se pudo eliminar de forma correcta"
        messages.error(request, mensaje)

    return redirect('listar_inmuebles')

def modificar(request, id):
    inmueble= Inmueble.objects.get(id=id)
    data= {
        'form':InmuebleForm(instance=inmueble)
    }
    if request.method == 'POST':
        formulario = InmuebleForm(data=request.POST, instance=inmueble, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Modificado de forma correcta"
        data['form'] = formulario

    return render(request,'core/modificar.html', data)

def registrar(request):
    data = {
        'form':CustomUserForm()
    }

    if  request.method == 'POST':
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            # autenticar y redireccionar al usuario
            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect(to='/')

    return render(request,'registration/registrar.html',data)

