# Generated by Django 2.2.3 on 2019-11-05 02:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_inmueble_dpto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inmueble',
            name='numero',
            field=models.IntegerField(),
        ),
    ]
