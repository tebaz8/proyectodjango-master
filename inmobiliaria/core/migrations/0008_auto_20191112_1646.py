# Generated by Django 2.2.7 on 2019-11-12 19:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20191108_2131'),
    ]

    operations = [
        migrations.CreateModel(
            name='Descripcion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descrip', models.CharField(max_length=200)),
                ('piezas', models.IntegerField()),
                ('banos', models.IntegerField()),
                ('metros', models.IntegerField()),
            ],
        ),
        migrations.RemoveField(
            model_name='zona',
            name='comuna',
        ),
        migrations.RemoveField(
            model_name='zona',
            name='provincia',
        ),
        migrations.AlterField(
            model_name='inmueble',
            name='descripcion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Descripcion'),
        ),
    ]
