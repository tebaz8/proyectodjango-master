# Generated by Django 2.2.7 on 2019-11-17 21:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20191116_2202'),
    ]

    operations = [
        migrations.AddField(
            model_name='inmueble',
            name='precio',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
