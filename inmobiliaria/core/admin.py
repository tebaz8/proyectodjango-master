from django.contrib import admin
from .models import Comuna,Inmueble,Provincia,Region,TipoInmueble,Zona,Nuevo

# Register your models here.

class InmobiliariaAdmin(admin.ModelAdmin):
    list_display=('region','provincia','comuna')
    search_fields=['comuna','region','provincia']    

admin.site.register(Comuna)
admin.site.register(Inmueble, InmobiliariaAdmin)
admin.site.register(Provincia)
admin.site.register(Region)
admin.site.register(TipoInmueble)
admin.site.register(Zona)
admin.site.register(Nuevo)



