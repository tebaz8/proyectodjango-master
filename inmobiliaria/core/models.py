from django.db import models

# Create your models here.




class Region(models.Model):
    nombre= models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.nombre

class Provincia(models.Model):
    nombre= models.CharField(max_length=50,null=False,blank=False)
    region=models.ForeignKey(Region,on_delete=models.CASCADE)  

    def __str__(self):
        return self.nombre  

class Comuna(models.Model):
    nombre=models.CharField(max_length=50,null=False,blank=False)    
    provincia=models.ForeignKey(Provincia,on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class Zona(models.Model):
    nombre=models.CharField(max_length=50,null=False,blank=False)
    region=models.ForeignKey(Region,on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class TipoInmueble(models.Model):
    tipo=models.CharField(max_length=50,null=False,blank=False)

    def __str__(self):
        return self.tipo

class Nuevo(models.Model):
    nuevo=models.CharField(max_length=2,null=False,blank=False)

    def __str__(self):
        return self.nuevo

class Inmueble(models.Model):
    
    precio=models.IntegerField(null=False,blank=False)  
    calle=models.CharField(max_length=50,null=False,blank=False)
    numero=models.IntegerField(null=False,blank=False)
    dpto=models.IntegerField(null=True,blank=True)    
    piezas=models.IntegerField(null=False,blank=False)
    banos=models.IntegerField(null=False,blank=False)
    metros=models.IntegerField(null=False,blank=False)
    nuevo=models.ForeignKey(Nuevo,on_delete=models.CASCADE)
    region=models.ForeignKey(Region,on_delete=models.CASCADE)
    provincia=models.ForeignKey(Provincia,on_delete=models.CASCADE)
    comuna=models.ForeignKey(Comuna,on_delete=models.CASCADE)
    tipo=models.ForeignKey(TipoInmueble,on_delete=models.CASCADE)
    imagen=models.ImageField(null=True, blank=True)
    descripcion=models.TextField(max_length=200,null=True,blank=True)

    def __str__(self):
        return self.calle
    