from django.urls import path
from .views import home,listar_inmuebles, eliminar_inmueble,modificar,nuevoFormulario, registrar

urlpatterns = [
    path('', home, name="home"),
    path('listar_inmuebles/',listar_inmuebles,name= "listar_inmuebles"),
    path('eliminar_inmueble/<id>/',eliminar_inmueble,name="eliminar_inmueble"),
    path('modificar/<id>/',modificar,name="modificar"),
    path('nuevoFormulario/',nuevoFormulario,name="nuevoFormulario"),
    path('registrar/',registrar,name="registrar"),
]