from django import forms
from django.forms import ModelForm
from .models import Inmueble
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class InmuebleForm(ModelForm):

    class Meta:
        model= Inmueble
        fields=['precio','calle','numero','dpto','piezas','banos','metros','nuevo','region','provincia','comuna','tipo','imagen','descripcion']


class CustomUserForm(UserCreationForm):
    
    class Meta:
        model = User
        fields=['first_name','last_name','email','username','password1','password2']